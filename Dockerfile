FROM golang:1.7.3
WORKDIR /go/src/
COPY / .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .
RUN ls

FROM scratch
WORKDIR /
COPY --from=0 /go/src/app /
EXPOSE 80
CMD ["./app"]