/*
Kubernetes Deployment
*/
package main // import "gitlab.com/tulpenhaendler/kube-basic"




import (
	"net/http"
	"time"
	"fmt"
	"sync"
)


func serveTime(w http.ResponseWriter, r *http.Request){
	response := "The time is now " +  time.Now().String()
	fmt.Fprintf(w, response)
}

func main(){
	wg := sync.WaitGroup{}
	wg.Add(1)
	http.HandleFunc("/", serveTime)
	http.ListenAndServe(":80",nil)


	wg.Wait()
}